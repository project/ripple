<?php

function ripple_field_info() {
  $items['ripple_currency'] = array(
    'label' => t('Ripple currency'),
    'description' => t('Ripple currency'),
    'default_widget' => 'ripple_field_currency',
    'default_formatter' => 'ripple_field_currency',
  );
  return $items;
}

function ripple_field_schema($field) {
  $items = array();
  $type = $field['type'];
  if ($type == 'ripple_currency') {
    $items['columns'] = array(
      'currency' => array(
        'description' => 'Short currency name i.e. USD or XRP.',
        'type' => 'varchar',
        'length' => 3,
        'not null' => TRUE,
      ),
      'issuer' => array(
        'description' => 'Ripple account id starts with r...',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
    );
    $items['indexes'] = array(
      'currency' => array('currency'),
      'issuer' => array('issuer'),
    );
  }
  elseif ($type == 'ripple_amount') {
    $items = array(
      'columns' => array(
        'value' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'currency' => array(
          'type' => 'varchar',
          'length' => 3,
          'not null' => TRUE,
        ),
        'issuer' => array(
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE, // NULL OK when currency == XRP
        ),
      ),
      'indexes' => array(
        'currency' => array('currency', 'issuer'),
      ),
    );
  }
  return $items;
}

function ripple_field_validate($entity_type, $entity, $field, $incance, $langcode, $items, &$errors) {
  //dpm(func_get_args(), __FUNCTION__);
  // @todo
}

function ripple_field_is_empty($item, $field) {
  // @todo
}

function ripple_field_formatter_info() {
  $items['ripple_currency'] = array(
    'label' => t('Ripple currency'),
    'field types' => array('ripple_currency'),
  );
  return $items;
}

function ripple_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $output = array();

  foreach ($items as $delta => $item) {
    $func = 'ripple_format_' . $display['type'];
    if (function_exists($func)) {
      // Our module knows how to format this element.
      $output[$delta] = array(
        '#prefix' => '<div class="' . $display['type'] . '">', '#suffix' => '</pre>',
        'value' => $func($item),
      );
      /*
      $output[$delta] = array(
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => array(
          'class' => array($display['type']),
        ),
        'value' => $func($item),
      );
      */
    }
    else {
      // Debug
      $output[$delta] = array(
        '#type' => 'html_tag',
        '#tag' => 'pre',
        '#value' => print_r($items[$delta], 1),
        '#attributes' => array(
          'class' => array($display['type']),
        ),
      );
    }
  }
  return $output;
}

function ripple_format_ripple_currency($item) {
  $output = array(
    '#prefix' => '<div class="ripple_currency ripple_currency_' . $item['currency'] . ' ripple_currency_' . $item['issuer'] . '">',
    '#suffix' => '</div>',
    'currency' => array(
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => array('class' => array('ripple_currency_currency')),
      '#value' => $item['currency'],
    ),
  );
  if (!empty($item['issuer']) && $item['issuer'] != $item['currency']) {
    $output['issuer'] = array(
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => array(
        'class' => array('ripple_currency_issuer'),
        'data-ripple-account' => $item['issuer'],
      ),
      '#value' => $item['issuer'],
    );
  }

  return $output;
}

function ripple_field_widget_info() {
  // Because we're smart, we have form element for each of our types.  Because Drupal is stupid, we still have to implement this hooks and hook_field_widget_form() just to map identical names to one another.
  $items['ripple'] = array(
    'label' => t('Ripple form element'),
    'field types' => array('ripple_currency', 'ripple_amount'),
  );
  return $items;
}

function ripple_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  $widget = $element;
  $widget['#delta'] = $delta;

  if ($instance['widget']['type'] == 'ripple') {
    // Note that our field types use same naming convention as our form elements.  (Cause that's how all Drupal modules should do it).
    $widget['#type'] = $field['type'];
    if (!empty($items[$delta])) {
      $widget['#default_value'] = $items[$delta];
    }
  }

  return $widget;
}
